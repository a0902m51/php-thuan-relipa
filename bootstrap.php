<?php

require_once ('libs/phpmailer/class.smtp.php');
require_once ('libs/phpmailer/class.phpmailer.php');
require_once ('core/controller.php');
require_once ('core/view.php');
require_once ('core/model.php');
require_once ('core/route.php');
require_once ('config/app.php');
require_once ('helpers.php');

Route::start();