<?php

session_start();
create_captcha_image();
exit();

function create_captcha_image()
{

    $md5_hash = md5(rand(0, 999));
    $security_code = substr($md5_hash, 15, 10);
    $_SESSION["captcha"] = $security_code;
    $width = 120;
    $height = 30;
    $image = ImageCreate($width, $height);
    $white = ImageColorAllocate($image, 255, 255, 255);
    $black = ImageColorAllocate($image, 9, 195, 241);
    ImageFill($image, 0, 0, $black);
    ImageString($image, 5, 10, 8, $security_code, $white);
    header("Content-Type: image/jpeg");
    ImageJpeg($image);
    ImageDestroy($image);
}