<?php

define('APP_NAME', 'Relipa');

const ROUTE = [
    'home' => '/home',
    'search' => '/home/search',
    'login' => '/auth/login',
    'logout' => '/auth/logout',
    'register' => '/auth/register',
    'check-login' => '/auth/checkLogin',
    'store' => '/auth/store',
    'forgot' => '/auth/forgot',
    'getEmail' => '/auth/getEmail'
];

const PAGE_ITEM = 25;

const GENDER_LIST = [
  1 => 'Male',
  2 => 'Female',
  3 => 'Other',
];

include_once ('mail.php');