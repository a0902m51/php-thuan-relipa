<?php

class AuthController extends Controller
{
    private $service;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new AuthServices();
    }

    /**
     * show login form with token
     */
    public function login()
    {
        if (isset($_SESSION['logged'])) {
            $this->view->redirect(getRoute('home'));
        }

        $data['token'] = createToken();
        $data['old'] = $this->getOldField();

        $this->view->load('login', $data);
    }

    /**
     * check account login
     */
    public function checkLogin()
    {
        $this->checkToken($_POST['_token']);
        $account = isset($_POST['account']) ? $_POST['account'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';

        $checkFlag = $this->service->checkLogin($account, $password);
        $url = getRoute('login');

        if ($checkFlag) {
            $_SESSION['logged'] = 'logged';
            $url = getRoute('home');
        }

        $this->view->redirect($url);
    }

    /**
     * default action
     */
    public function index()
    {
        $this->view->redirect(getRoute('login'));
    }

    /**
     * logout
     */
    public function logout()
    {
        if (isset($_SESSION['logged'])) {
            unset($_SESSION['logged']);
        }

        $this->view->redirect(getRoute('login'));
    }

    /**
     * show register form
     */
    public function register()
    {
        $data['token'] = createToken();

        $data['old'] = $this->getOldField();

        $this->view->load('register', $data);
    }

    /**
     * store new account
     */
    public function store()
    {
        $this->checkToken($_POST['_token']);

        $flag = $this->service->storeNewAccount($_POST);

        $url = getRoute('register');

        if ($flag) {

            $_SESSION['success'] = 'Your account created success.Sign in now.';

            $url = getRoute('login');
        }

        $this->view->redirect($url);
    }

    /**
     * show forgot page
     */
    public function forgot()
    {
        $data['token'] = createToken();

        $data['old'] = $this->getOldField();


        $this->view->load('forgot', $data);
    }

    /**
     * get Email forgot password
     */
    public function getEmail(){
        $this->checkToken($_POST['_token']);

        $result = $this->service->validForgotAccount($_POST);

        if(count($result) > 0){
            $this->service->getMail($result[0]['email'], $result[0]['password']);
        }

        $this->view->redirect(getRoute('forgot'));
    }
}