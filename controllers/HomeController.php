<?php

class HomeController extends Controller
{
    private $service;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new HomeServices();
    }

    /**
     * Home page
     */
    public function index()
    {

        $accounts = $this->service->getAccountList();

        $data['token'] = createToken();

        $data['accounts'] = $accounts;

        $this->view->load('home', $data);
    }

    /**
     * search
     */
    public function search(){
        $accounts = $this->service->search($_POST);

        echo json_encode($accounts);
    }
}