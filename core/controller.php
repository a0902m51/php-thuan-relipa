<?php

class Controller{

    protected $view;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * validate token
     * @param $token
     */
    public function checkToken($token){
        if($token != $_SESSION['token']){
            Route::error();
        }
    }

    /**
     * get old field if has error
     * * @return array
     */
    public function getOldField(){
        $data = null;

        if(isset($_SESSION['old_field'])){
            $old_field = $_SESSION['old_field'];
            unset($_SESSION['old_field']);
            $data = $old_field;
        }

        return $data;
    }

}