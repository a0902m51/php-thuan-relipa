<?php

include_once('config/database.php');

class BaseModel
{

    protected $host = DB_HOST;

    protected $user = DB_USER;

    protected $password = DB_PASSWORD;

    protected $database = DB_DATABASE;

    protected $table = '';

    protected $primary_key = 'id';

    /**
     * Make a connection
     * @return mysqli
     * @throws Exception
     */
    protected function connect()
    {
        $conn = new mysqli($this->host, $this->user, $this->password, $this->database);
        mysqli_set_charset($conn, 'utf8');
        if ($conn) {
            return $conn;
        }

        throw new Exception('Can not connect to database.');
    }

    /**
     * Close connection
     * @param $conn
     */
    protected function close($conn)
    {
        if ($conn) {
            $conn->close();
        }
    }

    /**
     * @param $sql
     * @param string $types
     * @param array $params
     * @return array
     * @throws Exception
     */
    protected function query($sql, $types = '', $params = [])
    {
        $conn = $this->connect();
        $data = [];

        if (!empty($types) && !empty($params)) {
            $stmt = $conn->prepare($sql);

            if ($types && $params) {
                $bind_names[] = $types;
                for ($i = 0; $i < count($params); $i++) {
                    $bind_name = 'bind' . $i;
                    $$bind_name = $params[$i];
                    $bind_names[] = &$$bind_name;
                }
                call_user_func_array([$stmt, 'bind_param'], $bind_names);
            }

            if ($stmt->execute()) {
                $result = $stmt->get_result();

                while ($record = $result->fetch_assoc()) {
                    $data[] = $record;
                }
            }

            $stmt->free_result();
            $stmt->close();
        } else {

            $data = $this->simpleQuery($conn, $sql);
        }

        $this->close($conn);

        return $data;
    }

    /**
     * run a simple select query without parameter
     * @param $conn
     * @param $sql
     * @return array
     * @throws Exception
     */
    private function simpleQuery($conn, $sql)
    {
        $data = [];

        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($record = mysqli_fetch_assoc($result)) {
                $data[] = $record;
            }
        }

        return $data;
    }

    /**
     * count total row
     * @throws Exception
     * @return mixed
     */
    protected function count()
    {
        $conn = $this->connect();

        $result = $conn->query("SELECT COUNT(*) AS num_row FROM {$this->table}")->fetch_assoc();

        $this->close($conn);

        return $result['num_row'];

    }

    /**
     * get collection with paginate
     * @param $page_item
     * @param $page
     * @param array $columns
     * @return array
     */
    public function paginate($page_item, $page = 1, $columns = [])
    {
        $conn = $this->connect();
        $data = [];

        $columns = $this->getColumns($columns);
        $offset = $this->getOffset($page_item, $page, $this->count());

        $result = $conn->query("SELECT {$columns} FROM {$this->table} LIMIT {$page_item} OFFSET {$offset}");
        if ($result->num_rows > 0) {
            while ($record = mysqli_fetch_assoc($result)) {
                $data[] = $record;
            }
        }

        $this->close($conn);

        return $data;
    }

    /**
     * split columns array to column
     * @param array $columns
     * @return string
     */
    public function getColumns($columns = [])
    {
        if (is_array($columns) && !empty($columns)) {
            $columns = implode(',', $columns);
        } else {
            $columns = "*";
        }

        return $columns;
    }

    /**
     * get offset
     * @param $item_per_page
     * @param $page
     * @param $total_row
     * @return int
     */
    protected function getOffset($item_per_page, $page, $total_row)
    {
        if ((int)$page > 0 && $page <= ceil($total_row / $item_per_page)) {
            $current_page = (int)$page;
        } else {
            $current_page = 1;
        }

        $offset = ($current_page - 1) * $item_per_page;

        return $offset;
    }

    /**
     * insert
     * @param $sql
     * @return bool
     */
    public function insert($sql)
    {
        $conn = $this->connect();
        $result = $conn->query($sql);

        $this->close($conn);

        return $result;
    }

    /**
     * @param array $array
     * @return mixed
     * @throws Exception
     */
    public function findBy($array = [])
    {
        $data = [];
        if (!empty($array)) {
            $conn = $this->connect();

            $type = "";
            $condition = "";
            $params = [];
            foreach ($array as $key => $value) {
                $type .= 's';
                $condition = sprintf("%s %s", $condition, "AND {$key}=?");
                $params[] = "{$value}";
            }

            $sql = "SELECT * FROM {$this->table} WHERE 1=1";
            $sql = sprintf('%s %s', $sql, $condition);

            $data = $this->query($sql, $type, $params);

            $this->close($conn);
        }

        return $data;
    }
}