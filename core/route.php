<?php

class Route
{
    static function start()
    {
        $config = require_once ('config/config.php');
        $controller_name = $config['controller_default'];
        $action = $config['action_default'];

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // get controller name
        if (!empty($routes[1])) {
            $controller_name = ucfirst($routes[1]);
        }

        // get name action
        if (!empty($routes[2])) {
            $action = $routes[2];
        }

        Route::loadModel($controller_name);

        Route::loadService($controller_name);

        Route::loadController($controller_name, $action);
    }

    /**
     * show error
     * @return bool
     */
    static function error()
    {
        include('404.php');

        return false;
    }

    /**
     * load service of controller
     * @param $name : controller name
     */
    private static function loadService($name){
        $service_name = sprintf('%sServices', $name);
        $service_file = sprintf("%s.php", $service_name);
        $service_path = sprintf('services/%s', $service_file);

        if(!file_exists($service_path)){
            Route::error();
        }

        include($service_path);;
    }

    /**
     * load controller
     * @param $controller_name : controller name
     * @param $action : action name
     */
    private static function loadController($controller_name, $action){

        $controller_name = sprintf('%sController', $controller_name);
        $controller_file = sprintf("%s.php", $controller_name);
        $controller_path = sprintf('controllers/%s', $controller_file);

        if (!file_exists($controller_path)) {
            Route::error();
        }

        include($controller_path);

        // create controller
        $controller = new $controller_name;

        if (method_exists($controller, $action)) {
            // call action of controller
            $controller->$action();
        } else {
            // redirect to 404 page
            Route::error();
        }
    }

    /**
     * load model of controller
     * @param $name : controller name
     */
    private static function loadModel($name){
        $model_name = sprintf('%sModel', $name);
        $model_file = sprintf("%s.php", $model_name);
        $model_path = sprintf('models/%s', $model_file);

        if(!file_exists($model_path)){
            Route::error();
        }

        include($model_path);
    }
}