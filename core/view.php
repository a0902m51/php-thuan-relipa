<?php

class View{
    private $__content = array();

    /**
     * Load view
     *
     * @param 	string
     * @param   array
     */
    public function load($view, $data = [])
    {
        $file = sprintf('views/%s.php', $view);

        if(!is_file($file)){
            Route::error();
        }

        extract($data);

        ob_start();
        require_once ($file);
        $content = ob_get_contents();

        ob_end_clean();

        $this->__content[] = $content;

        $this->render();
    }

    /**
     * render view
     */
    private function render()
    {
        foreach ($this->__content as $html){
            echo $html;
        }
    }


    /**
     * redirect page
     * @param $url
     */
    public function redirect($url){

        header("Location:{$url}");
    }
}