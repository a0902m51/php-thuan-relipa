CREATE DATABASE IF NOT EXISTS relipa;

USE relipa;

CREATE TABLE IF NOT EXISTS r_account (
  id INT(11) AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(128) NOT NULL,
  last_name  VARCHAR(128),
  address    VARCHAR(256),
  phone      VARCHAR(11),
  gender INT(1) DEFAULT 1,
  email VARCHAR(256) NOT NULL,
  username VARCHAR(48) NOT NULL,
  password VARCHAR(48) NOT NULL
) CHARACTER SET UTF8 ENGINE=InnoDB;
ALTER TABLE r_account AUTO_INCREMENT = 1001;

INSERT INTO r_account (first_name, last_name, address, phone, gender, email, username, password) VALUES('admin', '', '', '', 1, 'admin@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3');