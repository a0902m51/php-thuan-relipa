<?php

if(!function_exists('appName()')){
    /**
     * print app name
     */
    function appName(){
        return APP_NAME;
    }
}

if(!function_exists('createToken')){
    /**
     * get new token
     */
    function createToken(){
        $token  =  md5(uniqid(rand(),  TRUE));
        $_SESSION['token'] = $token;
        return $token;
    }
}

if(!function_exists('getRoute')){

    /**
     * get route
     * @param $key
     * @return string
     */
    function getRoute($key){
        $config = ROUTE;

        if(array_key_exists($key, $config)){
            return $config[$key];
        }

        return '';
    }
}