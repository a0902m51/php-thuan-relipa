<?php

class AuthModel extends BaseModel{
    protected $table = 'r_account';

    /**
     * check account login
     * @param $account
     * @param $password
     * @return mixed
     */
    public function checkLogin($account, $password){
        $password = md5($password);
        $params[] = "{$account}";
        $params[] = "{$account}";
        $params[] = "{$password}";
        $types = 'sss';

        $sql = "SELECT * FROM {$this->table} WHERE (username=? OR email=?) AND password=?";

        $result = $this->query($sql, $types, $params);

        if(count($result) > 0){
            return true;
        }

        return false;
    }

    /**
     * create new account
     * @param $request
     * @return bool
     */
    public function create($request){
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $phone = $request['phone'];
        $address = $request['address'];
        $username = $request['username'];
        $email = $request['email'];
        $password = md5($request['password']);

        $sql = "INSERT INTO {$this->table} (first_name, last_name,
                gender, phone, address, username, email, password)
                VALUES ('{$first_name}', '{$last_name}', {$gender}, '{$phone}', '{$address}', '{$username}', '{$email}', '{$password}')";

        return $this->insert($sql);
    }

    /**
     * @param $account
     * @return array
     */
    public function getAccount($account){
        $sql = "SELECT * FROM r_account WHERE username=? OR email=?";
        $params[]="{$account}";
        $params[]="{$account}";
        $types='ss';

        $results = $this->query($sql, $types, $params);

        return $results;
    }
}

