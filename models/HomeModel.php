<?php

class HomeModel extends BaseModel{
    protected $table = 'r_account';

    public function searchAccount($request, $columns = []){
        $username = isset($request['username']) ? $request['username'] : '';
        $email = isset($request['email']) ? $request['email'] : '';
        $page = isset($request['page']) ? $request['page'] : '';

        $total_row = $this->count();
        $offset = $this->getOffset(PAGE_ITEM, $page, $total_row);

        $columns = $this->getColumns($columns);

        $sql = "SELECT {$columns} FROM {$this->table} WHERE 1=1";

        $params = [];
        $types='';
        if(!empty($username)){
            $sql = sprintf('%s %s', $sql, "AND username LIKE ?");
            $types.='s';
            $params[] = "%{$username}%";
        }
        if(!empty($email)){
            $sql = sprintf('%s %s', $sql, "AND email LIKE ?");
            $params[] = "%{$email}%";
            $types.='s';
        }

        $sql = sprintf("%s LIMIT %s OFFSET %s", $sql, PAGE_ITEM, $offset);

        return $this->query($sql, $types, $params);
    }


}

