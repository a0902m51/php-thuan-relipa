$(function () {
	$('#form-search').submit(function (e) {
		e.preventDefault();

		var $form = $(this);
		var data = $form.serialize();
		var url = $form.attr('action');

		$.ajax({
			type: 'post',
			url: url,
			data: data,
			success: function (response) {
				var result = JSON.parse(response);
				var size = result.length;
				var html = "<tbody>";
				var table = $('#table-data');
				if (size > 0) {
					for (var i = 0; i < size; i++) {
						html += ""
							+ "<tr><td>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Account : </span>" + result[i].username
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Id : </span> " + result[i].id
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Full Name : </span>" + result[i].full_name
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Gender : </span>" + result[i].gender
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Email : </span>" + result[i].email
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12 pb-5'>"
							+ "<span class='title'>Phone : </span>" + result[i].phone
							+ "</div>"
							+ "<div class='col-md-12 col-sm-12 col-xs-12'>"
							+ "<span class='title'>Address : </span>" + result[i].address
							+ "</div>"
							+ "</td></tr>";
					}
				}
				else {
					html = "" +
						"<tr><td>NO RESULT</td></tr>";
				}

				html += "</tbody>";
				table.html(html);
			}
		});
	})
});
