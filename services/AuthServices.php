<?php

class AuthServices
{

    private $model;

    /**
     * AuthServices constructor.
     */
    public function __construct()
    {
        $this->model = new AuthModel();
    }


    /**
     * check account login
     * @param $account
     * @param $password
     * @return mixed
     */
    public function checkLogin($account, $password)
    {

        if (empty($account) || empty($password)) {
            $_SESSION['errors'][] = 'Please input your account and password.';
            $_SESSION['old_field']['account'] = $account;

            return false;
        }

        $checkFlag = $this->model->checkLogin($account, $password);

        if (!$checkFlag) {
            $_SESSION['errors'][] = 'Wrong account or password.';
            $_SESSION['old_field']['account'] = $account;

            return false;
        }

        return true;
    }

    /**
     * @param $request
     * @return bool
     * @throws Exception
     */
    public function storeNewAccount($request)
    {
        $valid = $this->validateStoreAccount($request);

        if ($valid) {
            if ($this->model->create($request) === true) {
                return true;
            } else {
                throw new Exception('Oops when wrong something.');
            }
        }

        return false;

    }

    /**
     * validate data account
     * @param $request
     * @return bool
     */
    private function validateStoreAccount($request)
    {
        $first_name = isset($request['first_name']) ? $request['first_name'] : '';
        $last_name = isset($request['last_name']) ? $request['last_name'] : '';
        $gender = isset($request['gender']) ? $request['gender'] : '';
        $phone = isset($request['phone']) ? $request['phone'] : '';
        $address = isset($request['address']) ? $request['address'] : '';
        $username = isset($request['username']) ? $request['username'] : '';
        $email = isset($request['email']) ? $request['email'] : '';
        $password = isset($request['password']) ? $request['password'] : '';
        $re_password = isset($request['re_password']) ? $request['re_password'] : '';
        $captcha = isset($request['captcha']) ? $request['captcha'] : '';

        if (empty($first_name)) {
            $_SESSION['errors']['first_name'] = 'First Name is required.';
        }

        if (empty($gender) || !key_exists($gender, GENDER_LIST)) {
            $_SESSION['errors']['gender'] = 'Gender is required.';
        }

        if (!empty($phone)) {
            $pattern = '/^[0-9]{10,11}$/';
            if (!preg_match($pattern, $phone)) {
                $_SESSION['errors']['phone'] = 'Phone is number.Phone numbers must be between 10 and 11 digits.';
            }
        }

        if (empty($username)) {
            $_SESSION['errors']['username'] = 'Username is required.';
        } else {
            $data = $this->model->findBy(['username' => $username]);
            if (count($data) > 0) {
                $_SESSION['errors']['username'] = 'Username has been exists.';
            }
        }

        if (empty($email)) {
            $_SESSION['errors']['email'] = 'Email is required.';
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                $_SESSION['errors']['email'] = 'Email is not valid.';
            } else {
                $data = $this->model->findBy(['email' => $email]);
                if (count($data) > 0) {
                    $_SESSION['errors']['email'] = 'Email has been exists.';
                }
            }
        }

        if (empty($password)) {
            $_SESSION['errors']['password'] = 'Please input password.';
        }

        if ($password != $re_password) {
            $_SESSION['errors']['re_password'] = 'Two password are not match.';
        }

        if ($_SESSION['captcha'] != $captcha) {
            $_SESSION['errors']['captcha'] = 'Captcha is not match.';
        }

        if (isset($_SESSION['errors']) && !empty($_SESSION['errors'])) {
            $_SESSION['old_field']['first_name'] = $first_name;
            $_SESSION['old_field']['last_name'] = $last_name;
            $_SESSION['old_field']['gender'] = $gender;
            $_SESSION['old_field']['phone'] = $phone;
            $_SESSION['old_field']['address'] = $address;
            $_SESSION['old_field']['username'] = $username;
            $_SESSION['old_field']['email'] = $email;

            return false;
        }

        return true;
    }


    /**
     * @param $email
     * @param $password
     * @return bool
     */
    public function getMail($email, $password)
    {
        if($this->mail($email, $password)){
            $_SESSION['success'] = 'Recover password mail has been sent.';

            return true;
        }

        $_SESSION['success'] = 'Exception.';
        return false;
    }

    /**
     * check account / email valid
     * @param $request
     * @return array
     */
    public function validForgotAccount($request)
    {
        $account = isset($request['account']) ? $request['account'] : '';
        $result = [];

        if (empty($account)) {
            $_SESSION['errors']['login'] = 'Please Enter your username / email.';

            return $result;
        }

        $result = $this->model->getAccount($account);

        if (count($result) <= 0) {
            $_SESSION['errors']['login'] = 'Account is not exists.';
            $_SESSION['old_field']['account'] = $account;

            return $result;
        }

        return $result;
    }

    /**
     * @param $to
     * @param $password
     * @return bool
     */
    public function mail($to, $password)
    {
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 1;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = SMTP_SECURE;
        $mail->Host = HOST_MAIL;
        $mail->Port = PORT_MAL;
        $mail->Username = USER_MAIL;
        $mail->Password = MAIL_API_KEY;
        $mail->setFrom(USER_MAIL, 'Recover Password');
        $mail->Subject = "Recovery Your Password ! Relipa";
        $mail->Body = "Your password : " . $password;
        $mail->AddAddress($to);

        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
}