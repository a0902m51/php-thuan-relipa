<?php

class HomeServices
{

    private $model;

    /**
     * AuthServices constructor.
     */
    public function __construct()
    {
        $this->model = new HomeModel();
    }

    /**
     * get account list
     * @param int $page
     * @return array
     */
    public function getAccountList($page = 1)
    {
        $accounts =  $this->model->paginate(PAGE_ITEM, $page);
        $data = $this->parseAccountsData($accounts);

        return $data;
    }

    /**
     * search
     * @param $request
     * @return array
     */
    public function search($request)
    {
        $accounts = $this->model->searchAccount($request);
        $data = $this->parseAccountsData($accounts);

        return $data;
    }

    /**
     * parse account data and entities data
     * @param $accounts
     * @return array
     */
    private function parseAccountsData($accounts)
    {
        $data = [];
        if(count($accounts) > 0){
            foreach ($accounts as $key => $account) {
                $data[$key]['username'] = htmlentities($account['username']);
                $data[$key]['id'] = $account['id'];
                $data[$key]['full_name'] = sprintf("%s%s",
                    ucfirst($account['first_name']),
                    ucfirst($account['last_name']));
                $gender = '';
                if (array_key_exists((int)$account['gender'], GENDER_LIST)) {
                    $gender = GENDER_LIST[(int)$account['gender']];
                }
                $data[$key]['gender'] = $gender;
                $data[$key]['email'] = htmlentities($account['email']);
                $data[$key]['phone'] = htmlentities($account['phone']);
                $data[$key]['address'] = htmlentities($account['address']);
            }
        }

        return $data;
    }
}