<?php

if(isset($_SESSION['errors']) && !empty($_SESSION['errors'])){
    $errors = $_SESSION['errors'];
    unset($_SESSION['errors']);

    echo "<div class='error'><ul>";
    foreach($errors as $error){
        echo "<li>{$error}</li>";
    }
    echo "</ul></div>";
}