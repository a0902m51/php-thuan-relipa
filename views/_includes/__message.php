<?php

if(isset($_SESSION['success']) && !empty($_SESSION['success'])){
    $success = $_SESSION['success'];
    unset($_SESSION['success']);

    echo "<div class='success'>";
    echo $success;
    echo "</div>";
}