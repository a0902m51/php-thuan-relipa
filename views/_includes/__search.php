<div class="search-box">
    <form action="<?= getRoute('search') ?>" method="post" id="form-search">
        <input name="_token" id="_token" value="<?=$token?>" type="hidden"/>
        <div class="row pb-5">
            <div class="col-md-4 col-sm-12 col-xs-12 pb-5">
                <input type="text" name="username" class="col-md-11" placeholder="Type account ...">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 pb-5">
                <input type="text" name="email" class="col-md-11" placeholder="Type email ...">
            </div>
            <div class="col-md-2 col-sm-12 col-xs-12">
                <input type="submit" id="btn-search" value="Search" class="btn-blue">
            </div>
        </div>
    </form>
</div>