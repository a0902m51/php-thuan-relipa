<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?= appName(); ?></title>

    <meta name="keywords" content="<?= appName(); ?>">
    <meta name="description" content="<?= appName(); ?>">
    <meta name="author" content="Scallar">

    <link rel="stylesheet" href="../public/css/common.css" type="text/css">
    <link rel="stylesheet" href="../public/css/site.css" type="text/css">
    <link rel="stylesheet" href="../public/css/login.css" type="text/css">
    <link rel="stylesheet" href="../public/css/tools.css" type="text/css">
</head>
<body>
<div class="container">
    <div class="row">
        <?php include_once ('_includes/__errors.php')?>
        <?php include_once ('_includes/__message.php')?>
        <form method="post" action="<?= getRoute('getEmail') ?>">
            <input type="hidden" name="_token" value="<?= $token ?>"/>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input <?php echo isset($old['account']) ? "value='{$old['account']}'" : ''?>
                        type="text" name="account" placeholder="Your account or email">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-10">
                    <input type="submit" class="btn-blue" value="Get email">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-20">
                    <a type="button" class="btn-primary" href="<?= getRoute('login'); ?>">To Sign in</a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>