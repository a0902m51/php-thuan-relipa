<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?= appName(); ?></title>

    <meta name="keywords" content="<?= appName(); ?>">
    <meta name="description" content="<?= appName(); ?>">
    <meta name="author" content="Scallar">

    <link rel="stylesheet" href="../public/css/common.css" type="text/css">
    <link rel="stylesheet" href="../public/css/site.css" type="text/css">
    <link rel="stylesheet" href="../public/css/tools.css" type="text/css">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
</head>
<body class="first">
    <div class="main">
        <div class="row pb-20">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <a href="<?= getRoute('logout') ?>" class="link-primary"> Logout </a>
            </div>
        </div>

        <div class="row pb-10">
            <?php include_once ('_includes/__search.php') ?>
        </div>

        <div class="row">
            <table class="table-style-1" id="table-data">
                <tbody>
                <?php
                if(isset($accounts) && !empty($accounts)){
                    foreach ($accounts as $account) {
                ?>
                <tr>
                    <td>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Account : </span> <?= $account['username'];?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Id : </span> <?= $account['id'];  ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Full Name : </span>
                            <?= $account['full_name']; ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Gender : </span>
                            <?= $account['gender']; ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Email : </span> <?= $account['email'];  ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                            <span class="title">Phone : </span> <?= $account['phone'];  ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <span class="title">Address : </span> <?= $account['address'];  ?>
                        </div>
                    </td>
                </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <script src="../public/js/search.js"></script>
</body>
</html>