<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?= appName(); ?></title>

    <meta name="keywords" content="<?= appName(); ?>">
    <meta name="description" content="<?= appName(); ?>">
    <meta name="author" content="Scallar">

    <link rel="stylesheet" href="../public/css/common.css" type="text/css">
    <link rel="stylesheet" href="../public/css/site.css" type="text/css">
    <link rel="stylesheet" href="../public/css/login.css" type="text/css">
    <link rel="stylesheet" href="../public/css/tools.css" type="text/css">
</head>
<body>
    <div class="container">
        <div class="row">
            <?php include_once ('_includes/__errors.php')?>
            <?php include_once ('_includes/__message.php')?>
            <form method="post" action="<?= getRoute('check-login'); ?>">
                <input type="hidden" name="_token" value="<?= $token ?>"/>

                <div class="row pb-15">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input <?php echo isset($old['account']) ? "value='{$old['account']}'" : ''?>
                            type="text" name="account" placeholder="Your account or email">
                    </div>
                </div>

                <div class="row pb-15">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="password" name="password" placeholder="Your password">
                    </div>
                </div>

                <div class="row pb-20">
                    <a href="<?= getRoute('forgot');?>" class="link-primary">Forgot password ?</a>
                </div>

                <div class="row pb-20">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" class="btn-blue" value="Sign in">
                    </div>
                </div>

                <div class="row text-right">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        Do not have an account ? <a href="<?= getRoute('register'); ?>" class="link-primary">Sign up</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>