<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title><?= appName(); ?></title>

    <meta name="keywords" content="<?= appName(); ?>">
    <meta name="description" content="<?= appName(); ?>">
    <meta name="author" content="Scallar">

    <link rel="stylesheet" href="../public/css/common.css" type="text/css">
    <link rel="stylesheet" href="../public/css/site.css" type="text/css">
    <link rel="stylesheet" href="../public/css/login.css" type="text/css">
    <link rel="stylesheet" href="../public/css/tools.css" type="text/css">
</head>
<body>
<div class="container register">
    <div class="row">
        <?php include_once('_includes/__errors.php') ?>
        <form method="post" action="<?= getRoute('store'); ?>">
            <input type="hidden" name="_token" value="<?= $token ?>"/>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <span class="require">*</span> <label for="first_name">First Name</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input
                        <?php
                        echo isset($errors['first_name']) ? "class='error-input'" : '';
                        echo isset($old['first_name']) ? "value='{$old['first_name']}'" : '';?>
                        type="text" name="first_name" id="first_name" placeholder="(*) Your first name">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <label for="last_name">Last Name</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input
                        <?php echo isset($old['last_name']) ? "value='{$old['last_name']}'" : ''; ?>
                        type="text" name="last_name" id="last_name" placeholder="Your last name">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <span class="require">*</span><label for="gender">Gender</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <select name="gender" id="gender" <?php echo ($errors['gender']) ? "class='error-input'" : ''; ?>>
                        <option value="-1">(*) Are you ?</option>
                        <?php
                        foreach (GENDER_LIST as $key => $gender) {
                            if (isset($old['gender']) && $old['gender'] == $key) {
                                echo "<option selected='selected' value='{$key}'>{$gender}</option>";
                            } else {
                                echo "<option value='{$key}'>{$gender}</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <label for="phone">Phone</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input
                        <?php
                        echo isset($errors['phone']) ? "class='error-input'" : '';
                        echo isset($old['phone']) ? "value='{$old['phone']}'" : '';?>
                        type="text" name="phone" id="phone" placeholder="Your phone" maxlength="11">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <label for="address">Address</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input
                        <?php echo isset($old['address']) ? "value='{$old['address']}'" : ''; ?>
                        type="text" name="address" id="address" placeholder="Your address">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <span class="require">*</span> Username
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input <?php
                        echo isset($errors['username']) ? "class='error-input'" : '';
                        echo isset($old['username']) ? "value='{$old['username']}'" : ''?>
                        type="text" name="username" placeholder="Enter your username">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <span class="require">*</span><label for="email">Email</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input <?php
                    echo isset($errors['email']) ? "class='error-input'" : '';
                    echo isset($old['email']) ? "value='{$old['email']}'" : ''?>
                        type="text" name="email" id="email" placeholder="Enter your email">
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <label for="password">Password</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input <?php echo isset($errors['password']) ? "class='error-input'" :'';?>
                        type="password" name="password" id="password" placeholder="Your password" maxlength="32"/>
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-5">
                    <label for="re_password">Re Password</label>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <input <?php echo isset($errors['re_password']) ? "class='error-input'" :'';?>
                        type="password" name="re_password" id="re_password" placeholder="Re-Enter your password" maxlength="32"/>
                </div>
            </div>

            <div class="row pb-15">
                <div class="col-md-6 col-sm-6 col-xs-6 pb-5">
                    <input  <?php echo isset($errors['captcha']) ? "class='error-input'" :'';?>
                        name="captcha" type="text" placeholder="Enter captcha"/>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 pl-5">
                    <img class="captcha" src="../captcha.php" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-10">
                    <input type="submit" class="btn-blue" value="Sign up">
                </div>
            </div>

            <div class="row text-right">
                <div class="col-md-12 col-sm-12 col-xs-12 pb-20">
                    <a type="button" class="btn-primary" href="<?= getRoute('login'); ?>">To Sign in</a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>